v2.2.0 (January 26, 2019)
- Updated dependencies
- Added instructions for rootless operation

v2.1.6 (August 30, 2018)
- Fix crashes while in landscape

v2.1.5 (July 18, 2018)
- Added support for grabbing some pstore log files 
